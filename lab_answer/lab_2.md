## Apakah perbedaan antara JSON dan XML?
Extensible Markup Language atau XML adalah bahasa markup yang mempunyai tujuan untuk menyimpan dan mentransfer data

JavaScript Object Notation atau JSON adalah sebuah file format berupa teks yang bertujuan untuk menyimpan dan mentransfer data 
 
<br>

JSON | XML
------------ | -------------
Mempunyai tipe string, number, boolean | Hanya bertipe string
Data lebih mudah diakses sebagai object JSON | Diperlukan parsing terlebih dahulu
Isi data bisa berupa teks dan angka | Isi data bisa berupa teks, angka, gambar, bagan, grafik, dll
Tidak bisa melakukan penampilan | Bisa melakukan penampilan
Tidak bisa comment | Bisa melakukan comment
Kurang aman dibandingkan XML | Lebih aman dibandingkan JSON
Tidak mendukung namespaces | Mendukung namespaces
Merupakan bahasa yang mudah dipahami dan tidak repetitif | Merupakan bahasa yang verbose

<br>

## Apakah perbedaan antara HTML dan XML?
HyperText Markup Language atau HTML adalah bahasa markup yang membuat, menampilkan, dan menstruktur data atau konten pada tampilan web. Walaupun XML juga markup language, mempunyai fungsi utama untuk menyimpan dan mentransfer data atau dokumen.  

HTML mempunyai bahasa standarnya sendiri, sedangkan XML mempunyai bahasa standar yang mendefinisikan bahasa lain. Sebagai contoh XML sering dipakai sebagai komplomen dari HTML, yaitu XML yang menyimpan data dan transfer data lalu HTML yang mentruktur dan menampilkan data tersebut  
<br>
Referensi:  
https://www.guru99.com/json-vs-xml-difference.html  
upgrad.com/blog/html-vs-xml/ 