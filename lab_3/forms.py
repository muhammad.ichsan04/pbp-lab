from django.forms import ModelForm, DateInput
from .models import Friend

class dateInput(DateInput):
    input_type = 'date'
    
class FriendForm(ModelForm):
    class Meta:
        model = Friend
        fields = '__all__'
        widgets = {'DOB' : dateInput()}

