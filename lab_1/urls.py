from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'), 
    # TODO Add friends path using friend_list Views
    path('friends', views.friend_list, name='friends'),
    path('say_hello', views.say_hello, name='say_hello')
]
