import 'package:flutter/material.dart';

/* Reference:
 * Cara Membuat Form di Flutter: https://belajarflutter.com/tutorial-cara-membuat-form-di-flutter-lengkap/
 * Flutter: Creating custom color swatch for MaterialColor: https://medium.com/@filipvk/creating-a-custom-color-swatch-in-flutter-554bcdcb27f3
 */

void main() {
  runApp(MaterialApp(
    title: "Kelompok D06",
    theme: ThemeData(
        primarySwatch: createMaterialColor(Color(0xFF811112))
    ),

    home: LoginForm(),
  ));
}

MaterialColor createMaterialColor(Color color) {
  List strengths = <double>[.05];
  final swatch = <int, Color>{};
  final int r = color.red, g = color.green, b = color.blue;

  for (int i = 1; i < 10; i++) {
    strengths.add(0.1 * i);
  }
  strengths.forEach((strength) {
    final double ds = 0.5 - strength;
    swatch[(strength * 1000).round()] = Color.fromRGBO(
      r + ((ds < 0 ? r : (255 - r)) * ds).round(),
      g + ((ds < 0 ? g : (255 - g)) * ds).round(),
      b + ((ds < 0 ? b : (255 - b)) * ds).round(),
      1,
    );
  });
  return MaterialColor(color.value, swatch);
}

class LoginForm extends StatefulWidget {
  @override
  _LoginForm createState() => _LoginForm();
}
class _LoginForm extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFCAB7A1),
      appBar: AppBar(
        title: Text("Kelompok D06 | Login"),
      ),

      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        backgroundColor: Color(0xFFB29576),
        selectedItemColor: Color(0xFF811112),
        unselectedItemColor: Color(0xFFCAB7A1),

        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),

          BottomNavigationBarItem(
            icon: Icon(Icons.accessibility_new),
            label: 'Workout',
          ),

          BottomNavigationBarItem(
            icon: Icon(Icons.book),
            label: 'Recipe',
          ),

          BottomNavigationBarItem(
            icon: Icon(Icons.assessment),
            label: 'Covid-19',
          ),

          BottomNavigationBarItem(
            icon: Icon(Icons.chat),
            label: 'Forum',
          ),

          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            label: 'Login',
          ),
        ],
      ),

      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      fillColor: Colors.red,
                      hintText: "example@example.com",
                      labelText: "Email Adress",
                      icon: Icon(Icons.people),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please insert something';
                      }
                      return null;
                    },
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    obscureText: true,
                    decoration: new InputDecoration(
                      fillColor: Colors.red,
                      labelText: "Password",
                      icon: Icon(Icons.security),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please insert something';
                      }
                      return null;
                    },
                  ),
                ),

                RaisedButton(
                  child: Text(
                    "Submit",
                    style: TextStyle(color: Color(0xFFCAB7A1)),
                  ),
                  color: Color(0xFF811112),
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {}
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}