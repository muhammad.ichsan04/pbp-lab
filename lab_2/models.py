from django.db import models

# Create your models here.
class Note(models.Model):
    penerima = models.CharField(max_length=40)
    pengirim = models.CharField(max_length=40)
    judul = models.CharField(max_length=40)
    pesan = models.CharField(max_length=40)