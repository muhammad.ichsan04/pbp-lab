from django.forms import ModelForm, TextInput
from .models import Note

class textInput(TextInput):
    input_type = 'text'

class NoteForm(ModelForm):
    class Meta:
        model = Note
        fields = ['penerima', 'pengirim', 'judul', 'pesan']
        widgets = {'judul': textInput(), 'pesan':textInput()}
